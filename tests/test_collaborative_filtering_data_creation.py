from src.dataprocess import *
import pandas as pd
import os


def rename_categoric_numeric_columns(df):
    '''
    input: any pandas dataframe
    output: dataframe after object coding,codes
    '''  
    categorical = {c : '%s_categorical'%(c) for c in df.columns if df[c].dtype == 'object' }
    numeric = {c : '%s_numeric'%(c) for c in df.columns if df[c].dtype != 'object'}
    df = df.rename(columns = numeric)
    df = df.rename(columns = categorical)   
    cat_cols = [c for c in df.columns if c.endswith('_categorical')]
    cat_codes = []
    for c in  tqdm(cat_cols):
        df[c], mapping_index = pd.Series(df[c]).factorize()
        cat_codes.append(mapping_index) 
    return df,cat_codes


def data_test(proc_df,orig_df,codes):
    test_sample=orig_df[['Entry','go']].sample(1)
    if not orig_df[orig_df.Entry==test_sample['Entry'].values[0]]['go'].values[0]==None:
        entry_code=codes[0].values.tolist().index(test_sample['Entry'].values[0])
        go_codes=[]
        for v in test_sample['go'].values[0].split(';'):
            go_codes.append(codes[1].values.tolist().index(v.strip()))

        assert set(proc_df[proc_df['Entry_categorical']==entry_code]['go_categorical'].values.tolist())==set(go_codes),"data cods are wrong"

def test_collaborative_filtering_data_creation():
    merged_df=pd.read_parquet(load_raw_gene_data())
    if not os.path.isfile('data/collabrative_filtering_data.parquet'):
        df=merged_df[['Entry','go']]
        df=expand_list(df,'go','Entry',';') 

        df,codes=rename_categoric_numeric_columns(df)
        df.to_parquet('data/collabrative_filtering_data.parquet')
        save_to_pickle(codes,'data/codes_collabrative_filtering.pickle')
    else:
        df=pd.read_parquet('data/collabrative_filtering_data.parquet')
        codes=load_from_pickle('data/codes_collabrative_filtering.pickle')
    
    print('dataframe loaded, testing validity')

    for j in tqdm(range(100)):
        data_test(df,merged_df,codes)

    print('test passed. saving codes:\n data/codes_collabrative_filtering.pickle \n parquet dataframe: \n data/collabrative_filtering_data.parquet ')


