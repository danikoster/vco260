import pytest
from src import datadownload
from tests import test_paramaters
import pandas as pd
import time
import os

def test_gene():
    #download small set test that is exactly what was expected
    datadownload.Gene(datapath='tests/test_data',crops=test_paramaters.crop1)
    #wait because download is in background
    time.sleep(5)
    #verify download, update shape if needed
    assert pd.read_csv('tests/test_data/ryegrass.gene').shape==(133,22), "got unexpected number of genes from ncbi"    
    os.remove('tests/test_data/ryegrass.gene')


def test_uniprot():
    #download small set test that is exactly what was expected
    datadownload.Uniprot(datapath='tests/test_data',featurelist1=test_paramaters.featlist,crops=test_paramaters.crop1)
    #wait because download is in background
    time.sleep(5)
    #verify download, update shape if needed
    assert pd.read_csv('tests/test_data/ryegrass.uniprot').shape==(166,125), "got unexpected number of genes from uniprot"    
    os.remove('tests/test_data/ryegrass.uniprot')

def test_expression():
    #download small set test that is exactly what was expected
    datadownload.Expression(datapath='tests/test_data',latin_names=test_paramaters.latin_names,crops=test_paramaters.crop2)
    #wait because download is in background
    time.sleep(5)
    #verify download, update shape if needed
    assert datadownload.load_from_pickle('tests/test_data/tomato.exp')[0].shape==(11458, 6), "got unexpected number of genes from expression atlas"    
    os.remove('tests/test_data/tomato.exp')
    



    