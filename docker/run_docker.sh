nvidia-smi
if [ $? -eq 0 ]; then
    gpu_or_cpu='gpu'
else
    gpu_or_cpu='cpu'
    echo "no working gpu"
fi

if [ "$gpu_or_cpu" = "gpu" ]; then 
    to_test="nvidia-docker"
    echo "checking if $to_test is installed" >&2
    if ! [ -x "$(command -v $to_test)" ]; then
    echo "Error: $to_test is not installed." >&2
    else
        cd gpu
        docker build -t amitshalev/vco260_gpu .
        nvidia-docker run -it amitshalev/vco260_gpu python -c "import tensorflow as tf; \
            a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a'); \
            b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b'); \
            c = tf.matmul(a, b); \
            sess = tf.Session(config=tf.ConfigProto(log_device_placement=True)); \
            print(sess.run(c));"

    fi

else
        cd cpu
        docker build -t amitshalev/vco260_cpu .
        docker run -it amitshalev/vco260_cpu python -c "import tensorflow as tf; \
            a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a'); \
            b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b'); \
            c = tf.matmul(a, b); \
            sess = tf.Session(config=tf.ConfigProto(log_device_placement=True)); \
            print(sess.run(c));"    
fi
